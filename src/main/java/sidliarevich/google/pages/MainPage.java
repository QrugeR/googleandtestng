package sidliarevich.google.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends AbstractPage {
    protected final String PAGE_URL;

    @FindBy(id = "lst-ib")
    private WebElement searchFor;
    //@FindBy(xpath = "//div[@class='jsb']//input[1]")
    @FindBy(xpath = "//span[@class='_wtf _Qtf']")
    private WebElement searchButton;
    @FindBy(id = "gb_70")
    private WebElement signInButton;

    public MainPage(WebDriver driver, String url) {
        super(driver);
        PAGE_URL = url;
        PageFactory.initElements(this.driver, this);
    }

    public void open() {
        driver.get(PAGE_URL);
    }

    public SearchResultsPage searchFor(String string) {
        searchFor.sendKeys(string);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
        return new SearchResultsPage(this.driver);
    }

    public LoginPage goToSignIn() {
        signInButton.click();
        return new LoginPage(this.driver);
    }
}


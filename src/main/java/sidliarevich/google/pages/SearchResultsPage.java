package sidliarevich.google.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultsPage extends AbstractPage {

    //@FindBy(xpath = "id('rso')//cite") private List<WebElement> links;
    @FindBy(xpath = "(//cite[@class='_Rm bc'])[1]")
    private WebElement link;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public String getFirstLink() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(link));
        return link.getText();
    }

}

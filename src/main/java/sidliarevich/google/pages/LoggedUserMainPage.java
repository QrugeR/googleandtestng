package sidliarevich.google.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoggedUserMainPage extends AbstractPage
{
    //@FindBy(xpath = "id('gbw')//div[@class='gb_tc gb_hb gb_vf gb_R']")
    @FindBy(xpath = "//span[@id='gbi4']")
    private WebElement userIcon;

    @FindBy(id = "gb_71")
    private WebElement signOutButton;

    //@FindBy(xpath = "//div[@class='gb_ub']")
    @FindBy(xpath = "//span[@id='gbmpn']")
    private WebElement userFirstAndLastNameLabel;

    //@FindBy(xpath = "//a[@class='gb_Fa gb_Ee gbp1 gb_wb']")
    @FindBy(id = "gb_150")
    private WebElement myAccountButton;

    public LoggedUserMainPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public MainPage singOut(){
        userIcon.click();
        signOutButton.click();
        return new MainPage(this.driver,driver.getCurrentUrl());
    }

    public String getFirstAndLastName(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(userIcon));
        userIcon.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(userFirstAndLastNameLabel));
        return userFirstAndLastNameLabel.getText();
    }

    public AccountPage openAccountPage(){
        //userIcon.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(myAccountButton));
        myAccountButton.click();
        return new AccountPage(this.driver);
    }

}

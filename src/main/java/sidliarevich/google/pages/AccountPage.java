package sidliarevich.google.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Aleh_Sidliarevich on 10/14/2016.
 */
public class AccountPage extends AbstractPage
{
    @FindBy(xpath = "//div[@class='Ll1RQb']")
    private WebElement helloUserLabel;

    public AccountPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public boolean isLabelContainsUsername(String username){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(helloUserLabel));
        return helloUserLabel.getText().contains(username);
    }


}

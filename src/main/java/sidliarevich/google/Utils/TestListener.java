package sidliarevich.google.Utils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener
{
    private WebDriver driver = null;
    private DateFormat dateTimeFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");

    public void onTestFailure(ITestResult result)
    {
        String methodName = result.getName().toString().trim();
        String datetime = dateTimeFormat.format(new Date());
        takeScreenShot(methodName+"_"+datetime);
    }

    public void takeScreenShot(String fileName)
    {
        driver = Utils.DriverFactory.getDriver();
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try
        {
            FileUtils.copyFile(scrFile, new File(System.getProperty("user.home") + Utils.ScreenShotsFolderPath + fileName + ".png"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void onFinish(ITestContext context)
    {
    }

    public void onTestStart(ITestResult result)
    {
    }

    public void onTestSuccess(ITestResult result)
    {
    }

    public void onTestSkipped(ITestResult result)
    {
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result)
    {
    }

    public void onStart(ITestContext context)
    {
    }
}

package sidliarevich.google.Utils;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Utils {
    public static String ScreenShotsFolderPath = "~\\SCREENSHOTS\\";

    static {
        //System.setProperty("webdriver.chrome.driver", "D:\\WebDriver\\chromedriver.exe");
    }

    final static public class DriverFactory {
        private static WebDriver driver;

        public static WebDriver getDriver() {
            try {
                if (driver == null) {
                    DesiredCapabilities cap = new DesiredCapabilities();

//                    cap.setCapability("platformName","Android");
//                    cap.setCapability("deviceName", "3200115858929000");
//                    cap.setBrowserName("Chrome");

//                    cap.setCapability("platformName", "iOS");
//                    cap.setCapability("platformVersion", "9.3");
//                    cap.setCapability("browserName", "Safari");
//                    cap.setCapability("deviceName", "iPhone Simulator");

                    driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), DesiredCapabilities.safari());
                }
                return driver;
            } catch (Exception e) {
                System.out.println("EXCEPTION!!!111 " + e.getMessage());
                return null;
            }
        }

    }
}

package sidliarevich.google.steps;

import org.openqa.selenium.WebDriver;
import sidliarevich.google.Utils.Utils;
import sidliarevich.google.pages.AccountPage;
import sidliarevich.google.pages.LoggedUserMainPage;
import sidliarevich.google.pages.MainPage;
import sidliarevich.google.pages.SearchResultsPage;

import java.util.concurrent.TimeUnit;

public class Steps
{
    private String BASE_GOOGLE_URL;
    private WebDriver driver;
    private MainPage mainpage;
    private SearchResultsPage searchResultsPage;
    private LoggedUserMainPage loggedUserMainPage;

    public Steps(String baseUrl){
        BASE_GOOGLE_URL = baseUrl;
    }

    public Steps initBrowser()
    {
        driver = Utils.DriverFactory.getDriver();
//        driver.manage().deleteAllCookies();
//        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
        return this;
    }

    public void quitBrowser()
    {
        driver.quit();
    }

    public Steps openGoogle()
    {
        mainpage = new MainPage(this.driver, BASE_GOOGLE_URL);
        mainpage.open();
        return this;
    }

    public Steps searchFor(String string)
    {
        searchResultsPage = mainpage.searchFor(string);
        return this;
    }

    public String getFirstLink()
    {
        return searchResultsPage.getFirstLink();
    }

    public Steps login(String email, String pass){
        loggedUserMainPage = mainpage.goToSignIn().signIn(email, pass);
        return this;
    }
    public Steps logout(){
        mainpage = loggedUserMainPage.singOut();
        return this;
    }

    public String getUserLastAndFirstName(){
        return loggedUserMainPage.getFirstAndLastName();
    }

    public boolean checkUserNameOnAccountPage(String username){
        return loggedUserMainPage.openAccountPage().isLabelContainsUsername(username);
    }


    public WebDriver getDriver(){
        return this.driver;
    }

}
